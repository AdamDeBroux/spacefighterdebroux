
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	//game objects are activated after the amount of time elapsed is greater than or equal to the delay time.
	//not sure why they couldn't have just gone
	/*
	if(pGameTime->GetTimeElapsed()>=m_delayseconds)	
	*/
	//but, whatever
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		//increases the m_activationSeconds variable to reflect how long it has been active
		m_activationSeconds += pGameTime->GetTimeElapsed();
		//if it is not on screen and it has been more than 2 seconds, deactivate.
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}
	//this calls the update function called for all ships rather than just the enemy ship
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}