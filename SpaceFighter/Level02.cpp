

#include "Level02.h"
#include "BioEnemyShip.h"


void Level02::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 31;

	double xPositions[COUNT] =
	{
		0.35, 0.25, 0.7,
		0.45, 0.6, 0.2,
		0.25, 0.35, 0.5, 0.7, 0.6,
		0.45, 0.65, 0.55, 0.25, 0.3,
		0.2, 0.25, 0.65, 0.4, 0.35,
		0.75, 0.65, 0.25, 0.8, 0.25,
		0.8, 0.75, 0.5, 0.7, 0.45
	};

	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		3.5, 0.3, 0.3, 0.3, 0.3,
		3.75, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip* pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

